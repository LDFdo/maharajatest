package com.restApi.maharajaTest.repository;

import com.restApi.maharajaTest.entity.Programme;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProgramRepo extends JpaRepository<Programme, Long> {
}
