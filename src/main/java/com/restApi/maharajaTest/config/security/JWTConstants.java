package com.restApi.maharajaTest.config.security;

public class JWTConstants {
    public static final String JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";
    public static final String ISSUER = "COMTEC";
    public static final String SUBJECT = "LOGIN";
}
