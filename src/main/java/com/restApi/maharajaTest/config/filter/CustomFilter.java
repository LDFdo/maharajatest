package com.restApi.maharajaTest.config.filter;


import com.restApi.maharajaTest.config.security.JWTAuthenticator;
import io.jsonwebtoken.Claims;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class CustomFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "*");
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "*");

        String authorization = httpServletRequest.getHeader("authorization");
        String servletPath = httpServletRequest.getPathInfo();
        String httpMethod = httpServletRequest.getMethod();

        if (httpMethod.equalsIgnoreCase("OPTIONS")) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        boolean hasToIgnore = ignorePaths(httpServletRequest, httpServletResponse, filterChain, servletPath,
                "/register/login", "/register/registration", "/index.html");
        if (hasToIgnore) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        try {
            Claims claims = JWTAuthenticator.decodeJWT(authorization);
        } catch (Exception e) {
            try (PrintWriter out = httpServletResponse.getWriter()) {
                httpServletResponse.setStatus(401);
                httpServletResponse.setContentType("application/json");
                out.println("Invalid token");
            } catch (IOException i) {
                i.printStackTrace();
            }
            e.printStackTrace();

        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }

    @Override
    public void destroy() {
    }

    /**
     * @param httpServletRequest:  The container generated servlet request object which sends from client
     * @param httpServletResponse: The response that will be generated from the server
     * @param filterChain:         the filterChain of the request
     * @param servletPath:         the servlet rest controller path of the request
     * @param ignorePaths:         the paths which needs to be ignored for token authorization
     * @return whether the path is equals to be ignored or not
     * @throws IOException
     * @throws ServletException
     */
    private boolean ignorePaths(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                FilterChain filterChain, String servletPath, String... ignorePaths) throws IOException, ServletException {
        String[] ignorePathsArray = ignorePaths.clone();
        for (String ignorePath : ignorePathsArray) {
            if (ignorePath.equalsIgnoreCase(servletPath)) {
                return true;
            }
        }
        return false;
    }

}