package com.restApi.maharajaTest.rest;

import com.restApi.maharajaTest.dto.LoginDTO;
import com.restApi.maharajaTest.dto.RegistrationDTO;
import com.restApi.maharajaTest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/register")
@CrossOrigin
public class RegisterRest {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/registration", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity register(@RequestBody RegistrationDTO registrationDTO){
        System.out.println("work");
        try {
            registrationDTO = userService.register(registrationDTO);
            return ResponseEntity.ok(registrationDTO);
        }catch (RuntimeException rex){
            rex.printStackTrace();
            return new ResponseEntity<>("Error! " + rex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity login(@RequestBody LoginDTO loginDTO){
        try {
            loginDTO = userService.login(loginDTO);
            return ResponseEntity.ok(loginDTO);
        }catch (RuntimeException rex){
            rex.printStackTrace();
            return new ResponseEntity<>("Unauthorized!  " + rex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }
}
