package com.restApi.maharajaTest.rest;

import com.restApi.maharajaTest.dto.ProgramDTO;
import com.restApi.maharajaTest.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/program")
public class ProgramRest {

    @Autowired
    private ProgramService programService;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody ProgramDTO programDto){
        try{
            programDto = programService.create(programDto);
            return ResponseEntity.ok(programDto);
        }catch (RuntimeException rex){
            rex.printStackTrace();
            return new ResponseEntity<>("Error! " + rex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PostMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody ProgramDTO programDto){
        try{
            programDto = programService.update(programDto);
            return ResponseEntity.ok(programDto);
        }catch (RuntimeException rex){
            rex.printStackTrace();
            return new ResponseEntity<>("Error! " + rex.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity viewAll(@RequestParam("offset")Long offset, @RequestParam("start_date")String start_date,
                                  @RequestParam("end_date")String end_date, @RequestParam("limit")Long limit){

        List<ProgramDTO> programDTOS = programService.viewAll(offset,start_date,end_date, limit);
        return ResponseEntity.ok(programDTOS);

    }

}
