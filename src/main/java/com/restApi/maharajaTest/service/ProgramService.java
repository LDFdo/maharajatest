package com.restApi.maharajaTest.service;

import com.restApi.maharajaTest.dto.ProgramDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProgramService {

    ProgramDTO create(ProgramDTO programDTO);
    ProgramDTO update(ProgramDTO programDTO);
    List<ProgramDTO> viewAll(Long offset ,String start_date, String end_date, Long limit);
}
