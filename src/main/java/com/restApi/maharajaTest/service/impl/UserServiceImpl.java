package com.restApi.maharajaTest.service.impl;

import com.restApi.maharajaTest.config.security.JWTAuthenticator;
import com.restApi.maharajaTest.dto.LoginDTO;
import com.restApi.maharajaTest.dto.RegistrationDTO;
import com.restApi.maharajaTest.entity.User;
import com.restApi.maharajaTest.repository.UserRepo;
import com.restApi.maharajaTest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.restApi.maharajaTest.config.security.JWTConstants.ISSUER;
import static com.restApi.maharajaTest.config.security.JWTConstants.SUBJECT;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;

    @Override
    public RegistrationDTO register(RegistrationDTO registrationDTO) {
        User user = userRepo.findByUsername(registrationDTO.getUsername());
        if(user != null){
            throw new RuntimeException("Username exists ! try Another");
        }
        User users = new User();
        users.setName(registrationDTO.getName());
        users.setDate_of_birth(registrationDTO.getDate_of_birth());
        users.setMobile_number(registrationDTO.getMobile_number());
        users.setGender(registrationDTO.getGender());
        users.setLanguage(registrationDTO.getLanguage());
        users.setUsername(registrationDTO.getUsername());
        users.setPassword(registrationDTO.getPassword());

        users = userRepo.save(users);
        return registrationDTO;
    }

    @Override
    public LoginDTO login(LoginDTO loginDTO) {
        User user = userRepo.findByUsername(loginDTO.getUsername());
        if (user == null){
            throw new RuntimeException("User Not Found !");
        }
        if (!user.getPassword().equals(loginDTO.getPassword())){
            throw new RuntimeException("Invalid password");
        }
        String jwt = JWTAuthenticator.createJWT(loginDTO.getUsername(), ISSUER, SUBJECT, 86400000);
        loginDTO.setToken(jwt);
        return loginDTO;
    }
}
