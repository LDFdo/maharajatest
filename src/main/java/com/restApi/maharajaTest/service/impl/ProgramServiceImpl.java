package com.restApi.maharajaTest.service.impl;

import com.restApi.maharajaTest.dto.ProgramDTO;
import com.restApi.maharajaTest.entity.Programme;
import com.restApi.maharajaTest.repository.ProgramRepo;
import com.restApi.maharajaTest.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProgramServiceImpl implements ProgramService {

    @Autowired
    private ProgramRepo programRepo;

    @Override
    public ProgramDTO create(ProgramDTO programDTO) {
        Programme programme = new Programme();
        programme.setName(programDTO.getName());
        programme.setDescription(programDTO.getDescription());
        programme.setEnd_date(programDTO.getEnd_date());
        programme.setStart_date(programDTO.getStart_date());

        programme = programRepo.save(programme);
        return programDTO;

    }

    @Override
    public ProgramDTO update(ProgramDTO programDTO) {
        Optional<Programme> optionalProgramme = programRepo.findById(Long.parseLong(programDTO.getProgram_id()));
        if(!optionalProgramme.isPresent()){
            throw new RuntimeException("programme not found !");
        }
        Programme programme = optionalProgramme.get();
        programme.setName(programDTO.getName());
        programme.setDescription(programDTO.getDescription());
        programme.setStart_date(programDTO.getStart_date());
        programme.setEnd_date(programDTO.getEnd_date());

        programme = programRepo.save(programme);
        return programDTO;
    }

    @Override
    public List<ProgramDTO> viewAll(Long offset, String start_date, String end_date, Long limit) {
        ArrayList<ProgramDTO> programDTOS = new ArrayList<>();
        List<Programme> allProgramme = programRepo.findAll();
        allProgramme.forEach(pro -> {
            ProgramDTO programDTO = new ProgramDTO();
            programDTO.setName(pro.getName());
            programDTO.setDescription(pro.getDescription());
            programDTO.setStart_date(pro.getStart_date());
            programDTO.setEnd_date(pro.getEnd_date());
            programDTO.setProgram_id(String.valueOf(pro.getProgrammeId()));
            if (Date.valueOf(pro.getStart_date()).after(Date.valueOf(start_date))
                    && Date.valueOf(pro.getEnd_date()).before(Date.valueOf(end_date)))  {
                programDTOS.add(programDTO);
            }
        });
        return programDTOS;
    }
}
