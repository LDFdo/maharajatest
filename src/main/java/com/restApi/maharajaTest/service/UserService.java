package com.restApi.maharajaTest.service;

import com.restApi.maharajaTest.dto.LoginDTO;
import com.restApi.maharajaTest.dto.RegistrationDTO;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    RegistrationDTO register (RegistrationDTO registrationDTO);
    LoginDTO login(LoginDTO loginDTO);
}
