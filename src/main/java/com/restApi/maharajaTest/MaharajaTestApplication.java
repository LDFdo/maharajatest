package com.restApi.maharajaTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaharajaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaharajaTestApplication.class, args);
	}

}
